<a name="readme-top"></a>

# Drupal Cache Remember

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
This project is a testing ground that aims to provide an easier way to populate Drupal caches using a "remember" style cache interface similar to what you find in Laravel for Drupal.

Work for this was originally started in the [Drupal issue queue](https://www.drupal.org/project/drupal/issues/3013177), but development stalled. I think this is partially because it's hard to review the proposed changes in real cache scenarios. The plan for the project is to provide an easy way to use the interface in real world cases and build up some feedback to eventually provide back as a patch to Drupal.



There are two approaches captured in this repository.
  - The first, CacheRememberInterface, was the original approach in the Drupal Core issue. It has a drawback though because it requires calculating the expected expiry for every call because you don't know if the cache will hit or not. Because of the way Drupal treats expiry values, this requires retrieving the current time even when it isn't needed adding what is probably a measurable overhead and complexity to cache hits.
  - The second, SimpleCacheRememberInterface, is an updated approach that simplifies the interface in a lot of ways. It passes the cid, expire, and tags arguments to the callback allowing them to be calculated and set _only_ on cache misses. When coupled with named arguments this makes it much easier to accomplish common caching tasks in a much easier to understand way.


<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->
## Usage

You can get started using this project today by simply requiring it with composer.

```shell
composer require drupal/cache_remember
```

After that, any class you'd like to use this interface on you can simply use one of the helper traits on your class.

### CacheRememberInterface
```php

use \Neclimdul\DrupalCacheRemember\CacheRememberHelperTrait;

class OriginalApproach {
    use CacheRememberHelperTrait;

    public function __construct() {
        // Use a real backend for this. Probably something injected from the service container.
        $this->setCacheRememberBackend(new \Drupal\Core\Cache\MemoryBackend());
    }

    public function myFunction() {
        print $this->remember('test:cid', function() {
            return 'My test value';
        },  \Drupal::time()->getCurrentTime() + 600, ['custom:tag']);
        print $this->rememberPermanent('test:cid2', function() {
            return 'My test value';
        });
    }
}

```

### SimpleCacheRememberInterface

```php

use \Neclimdul\DrupalCacheRemember\SimpleCacheRememberHelperTrait;

class UpdatedApproach {
    use SimpleCacheRememberHelperTrait;

    public function __construct() {
        // Use a real backend for this. Probably something injected from the service container.
        $this->setCacheRememberBackend(new \Drupal\Core\Cache\MemoryBackend());
    }

    public function myFunction() {
        print $this->remember('test:cid', function($cid, &$expire, &$tags) {
            $expire = \Drupal::time()->getCurrentTime() + 600;
            $tags = ['custom:tag'];
            return 'My test value';
        });
        print $this->remember('test:cid2', function($cid, &$expire) {
            $expire = \Drupal::time()->getCurrentTime() + 600;
            return 'My test value';
        }, tags: ['custom:tag']);
    }
}

```

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

After sufficient testing, this will be provided as a patch to Drupal core because it makes more sense for this to be part of cache backends then bolted on to individual classes like this library will require.

https://www.drupal.org/project/drupal/issues/3013177

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTRIBUTING -->
## Contributing

Contributions and feedback would be greatly appreciated! Please submit any feedback, suggestions, or improvements to this project.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- LICENSE -->
## License

As this is targeting Drupal core, all changes _MUST_ be GPL 2.0 or they won't be able to be contributed back. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>
