<?php

namespace Neclimdul\DrupalCacheRemember\tests;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCache;
use Neclimdul\DrupalCacheRemember\CacheRememberHelperTrait;
use Neclimdul\DrupalCacheRemember\CacheRememberInterface;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\DrupalCacheRemember\CacheRememberTrait
 */
class CacheRememberTraitTest extends TestCase
{

  private CacheRememberInterface $sot;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $backend;

  public function setUp(): void
  {
    parent::setUp();
    $this->backend = new MemoryCache();
    $this->sot = new class($this->backend) implements CacheRememberInterface {
      use CacheRememberHelperTrait;

      public function __construct(CacheBackendInterface $backend) {
        $this->setCacheRememberBackend($backend);
      }
    };
  }

  /**
   * @covers ::remember
   */
  public function testRemember(): void {
    $expire = time() + 123;
    $test_value = 'test_value';
    $this->assertEquals($test_value,
      $this->sot->remember('cid', function () use ($test_value) {
        return $test_value;
      }, $expire)
    );
    $this->assertCacheResult(
      (object) [
        'data' => $test_value,
        'expire' => $expire,
        'tags' => [],
      ],
      $this->backend->get('cid')
    );

    // Expire in past to regenerate and store the tag.
    $this->backend->set('cid', '', 1);
    $test_value .= '-with_tags';
    $expire++;
    $this->assertEquals($test_value,
      $this->sot->remember('cid', function () use ($test_value) {
        return $test_value;
      }, $expire, ['cache:tag'])
    );
    $this->assertCacheResult(
      (object) [
        'data' => $test_value,
        'expire' => $expire,
        'tags' => ['cache:tag'],
      ],
      $this->backend->get('cid')
    );

    // Value in cache is not touched and returned.
    $this->assertEquals($test_value,
      $this->sot->remember('cid', function () {
        return 'new value';
      }, $expire + 1)
    );
    $this->assertCacheResult(
      (object) [
        'data' => $test_value,
        'expire' => $expire,
        'tags' => ['cache:tag'],
      ],
      $this->backend->get('cid')
    );
  }

  /**
   * @covers ::rememberPermanent
   */
  public function testRememberPermanent(): void {
    $test_value = 'test_value';
    $this->assertEquals($test_value,
      $this->sot->rememberPermanent('cid', function () use ($test_value) {
        return $test_value;
      })
    );
    $this->assertCacheResult(
      (object) [
        'data' => $test_value,
        'expire' => CacheBackendInterface::CACHE_PERMANENT,
        'tags' => [],
      ],
      $this->backend->get('cid')
    );

    // Expire in past to regenerate and store the tag.
    $this->backend->set('cid', '', 1);
    $test_value .= '-with_tags';
    $this->assertEquals($test_value,
      $this->sot->rememberPermanent('cid', function () use ($test_value) {
        return $test_value;
      }, ['cache:tag'])
    );
    $this->assertCacheResult(
      (object) [
        'data' => $test_value,
        'expire' => CacheBackendInterface::CACHE_PERMANENT,
        'tags' => ['cache:tag'],
      ],
      $this->backend->get('cid')
    );

    // Value in cache is not touched and returned.
    $this->assertEquals($test_value,
      $this->sot->rememberPermanent('cid', function () {
        return 'new value';
      })
    );
    $this->assertCacheResult(
      (object) [
        'data' => $test_value,
        'expire' => CacheBackendInterface::CACHE_PERMANENT,
        'tags' => ['cache:tag'],
      ],
      $this->backend->get('cid')
    );
  }

  private function assertCacheResult($expected, $actual): void {
    $this->assertEquals(
      $expected->expire,
      $actual->expire
    );
    $this->assertEquals(
      $expected->data,
      $actual->data
    );
    $this->assertEquals(
      $expected->tags,
      $actual->tags
    );
  }

}
