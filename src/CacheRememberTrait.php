<?php

namespace Neclimdul\DrupalCacheRemember;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Provides an implementation of CacheRememberInterface.
 *
 * @see \Neclimdul\DrupalCacheRemember\CacheRememberInterface
 */
trait CacheRememberTrait {

  /**
   * {@inheritDoc}
   */
  public function remember(string $cid, callable $callback, int $expire, array $tags = []): mixed {
    $cache = $this->get($cid);
    if (is_object($cache)) {
      return $cache->data;
    }
    $data = $callback();
    $this->set($cid, $data, $expire, $tags);
    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function rememberPermanent(string $cid, callable $callback, array $tags = []): mixed {
    return $this->remember($cid, $callback, CacheBackendInterface::CACHE_PERMANENT, $tags);
  }

}
