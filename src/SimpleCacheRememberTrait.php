<?php

namespace Neclimdul\DrupalCacheRemember;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Provides an implementation of SimpleCacheRememberInterface.
 *
 * @see \Neclimdul\DrupalCacheRemember\SimpleCacheRememberInterface
 */
trait SimpleCacheRememberTrait {

  /**
   * {@inheritDoc}
   */
  public function remember(string $cid, callable $callback, int $expire = CacheBackendInterface::CACHE_PERMANENT, array $tags = []): mixed {
    $cache = $this->get($cid);
    if (is_object($cache)) {
      return $cache->data;
    }
    $data = $callback($cid, $expire, $tags);
    $this->set($cid, $data, $expire, $tags);
    return $data;
  }

}
