<?php

namespace Neclimdul\DrupalCacheRemember;

/**
 * An interface for remember cache lookup helper methods.
 *
 * Remember methods provide helper methods for common fetch and set tasks. It
 * uses callbacks to allow the caller to define the logic that populates the
 * cache and abstracts the repetitive fetch logic that does not need to be
 * customized in most use cases.
 *
 * @ingroup cache
 */
interface CacheRememberInterface {

  /**
   * Remembers data in a persistent cache.
   *
   * Helper method for retrieving and storing data to a cache.
   *
   * @param string $cid
   *   The cache ID of the data to store.
   * @param callable $callback
   *    A callback that will be used to populate the cache in the case of a miss.
   * @param int $expire
   *   One of the following values:
   *   - CacheBackendInterface::CACHE_PERMANENT: Indicates that the item should
   *     not be removed unless it is deleted explicitly.
   *   - A Unix timestamp: Indicates that the item will be considered invalid
   *     after this time, i.e. it will not be returned by get() unless
   *     $allow_invalid has been set to TRUE. When the item has expired, it may
   *     be permanently deleted by the garbage collector at any time.
   * @param array<string> $tags
   *   An array of tags to be stored with the cache item. These should normally
   *   identify objects used to build the cache item, which should trigger
   *   cache invalidation when updated. For example if a cached item represents
   *   a node, both the node ID and the author's user ID might be passed in as
   *   tags. For example ['node:123', 'node:456', 'user:789'].
   *
   * @return mixed
   *    The data stored in the cache and returned from the generating callback.
   */
  public function remember(string $cid, callable $callback, int $expire, array $tags = []): mixed;

  /**
   * Remembers data in a persistent cache.
   *
   * Helper method for retrieving and storing data to a cache.
   *
   * @param string $cid
   *   The cache ID of the data to store.
   * @param callable $callback
   *   A callback that will be used to populate the cache in the case of a miss.
   * @param array<string> $tags
   *   An array of tags to be stored with the cache item. These should normally
   *   identify objects used to build the cache item, which should trigger
   *   cache invalidation when updated. For example if a cached item represents
   *   a node, both the node ID and the author's user ID might be passed in as
   *   tags. For example ['node:123', 'node:456', 'user:789'].
   *
   * @return mixed
   *    The data stored in the cache and returned from the generating callback.
   */
  public function rememberPermanent(string $cid, callable $callback, array $tags = []): mixed;

}
