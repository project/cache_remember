<?php

namespace Neclimdul\DrupalCacheRemember;

use Drupal\Core\Cache\CacheBackendInterface;

trait SimpleCacheRememberHelperTrait
{
  use SimpleCacheRememberTrait;

  private CacheBackendInterface $backend;

  public function setCacheRememberBackend(CacheBackendInterface $backend): void {
    $this->backend = $backend;
  }

  /**
   * @see \Drupal\Core\Cache\CacheBackendInterface::get
   */
  private function get(string $cid): bool|object {
    return $this->backend->get($cid);
  }

  /**
   * @see \Drupal\Core\Cache\CacheBackendInterface::set
   */
  private function set(string $cid, mixed $data, int $expire = CacheBackendInterface::CACHE_PERMANENT, array $tags = []): void {
    $this->backend->set($cid, $data, $expire , $tags);
  }

}
